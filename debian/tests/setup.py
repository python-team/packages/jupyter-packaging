from setuptools import setup
from jupyter_packaging import create_cmdclass, install_npm


cmdclass = create_cmdclass(['js'])
cmdclass['js'] = install_npm()

setup_args = dict(
    name             = 'TEST',
    cmdclass         = cmdclass,
)

if __name__ == '__main__':
    setup(**setup_args)
